package ua.barterio.testproject.objects;

import java.util.ArrayList;

public class MainObject {
	public static final String TAG = "main object";
	
	public static final String GUID = "guid";
	public static final String INDEX = "index";
	public static final String FAVORITE_FRUIT = "favoriteFruit";
	public static final String LATITUDE = "latitude";
	public static final String COMPANY = "company";
	public static final String EMAIL = "email";
	public static final String PICTURE = "picture";
	public static final String TAGS = "tags"; 				//
	public static final String REGISTERED = "registered";
	public static final String EYE_COLOR = "eyeColor";
	public static final String PHONE = "phone";
	public static final String ADDRESS = "address";
	public static final String FRIENDS = "friends"; 		//
	public static final String IS_ACTIVE = "isActive";
	public static final String ABOUT = "about";
	public static final String BALANCE = "balance";
	public static final String NAME = "name";
	public static final String GENDER = "gender";
	public static final String AGE = "age";
	public static final String GREETING = "greeting";
	public static final String LONGITUDE = "longitude";
	public static final String ID = "_id";
	public static final String TABLE_ID = "table_id";
	
	private String mGuid;
	private int mIndex;
	private String mFavoriteFruit;
	private long mLatitude;
	private String mCompany;
	private String mEmail;
	private String mPicture;
	private ArrayList<Tags> mTags;			 //
	private String mRegistered;
	private String mEyeColor;
	private String mPhone;
	private String mAddress;
	private ArrayList<Friends> mFriends;	//
	private boolean mIsActive;
	private String mAbout;
	private String mBalance;
	private String mName;
	private String mGender;
	private int mAge;
	private String mGreeting;
	private long mLongitude;
	private String mId;
	
	public MainObject(){
		mTags = new ArrayList<Tags>();
		mFriends = new ArrayList<Friends>();
	};
	
	public String getGuid() {
		return mGuid;
	}
	public void setGuid(String guid) {
		mGuid = guid;
	}
	public int getIndex() {
		return mIndex;
	}
	public void setIndex(int index) {
		mIndex = index;
	}
	public String getFavoriteFruit() {
		return mFavoriteFruit;
	}
	public void setFavoriteFruit(String favoriteFruit) {
		mFavoriteFruit = favoriteFruit;
	}
	public long getLatitude() {
		return mLatitude;
	}
	public void setLatitude(long latitude) {
		mLatitude = latitude;
	}
	public String getCompany() {
		return mCompany;
	}
	public void setCompany(String company) {
		mCompany = company;
	}
	public String getEmail() {
		return mEmail;
	}
	public void setEmail(String email) {
		mEmail = email;
	}
	public String getPicture() {
		return mPicture;
	}
	public void setPicture(String picture) {
		mPicture = picture;
	}
	public ArrayList<Tags> getTags() {
		return mTags;
	}
	public void setTags(ArrayList<Tags> tags) {
		mTags = tags;
	}
	public void addTag(Tags tag){
		mTags.add(tag);
	}
	public String getRegistered() {
		return mRegistered;
	}
	public void setRegistered(String registered) {
		mRegistered = registered;
	}
	public String getEyeColor() {
		return mEyeColor;
	}
	public void setEyeColor(String eyeColor) {
		mEyeColor = eyeColor;
	}
	public String getPhone() {
		return mPhone;
	}
	public void setPhone(String phone) {
		mPhone = phone;
	}
	public String getAddress() {
		return mAddress;
	}
	public void setAddress(String address) {
		mAddress = address;
	}
	public ArrayList<Friends> getFriends() {
		return mFriends;
	}
	public void setFriends(ArrayList<Friends> friends) {
		mFriends = friends;
	}
	public void addFriend(Friends friend){
		mFriends.add(friend);
	}
	public boolean isIsActive() {
		return mIsActive;
	}
	public void setIsActive(boolean isActive) {
		mIsActive = isActive;
	}
	public String getAbout() {
		return mAbout;
	}
	public void setAbout(String about) {
		mAbout = about;
	}
	public String getBalance() {
		return mBalance;
	}
	public void setBalance(String balance) {
		mBalance = balance;
	}
	public String getName() {
		return mName;
	}
	public void setName(String name) {
		mName = name;
	}
	public String getGender() {
		return mGender;
	}
	public void setGender(String gender) {
		mGender = gender;
	}
	public int getAge() {
		return mAge;
	}
	public void setAge(int age) {
		mAge = age;
	}
	public String getGreeting() {
		return mGreeting;
	}
	public void setGreeting(String greeting) {
		mGreeting = greeting;
	}
	public long getLongitude() {
		return mLongitude;
	}
	public void setLongitude(long longitude) {
		mLongitude = longitude;
	}
	public String getId() {
		return mId;
	}
	public void setId(String id) {
		mId = id;
	}

	@Override
	public String toString() {
		return "MainObject [mGuid=" + mGuid + ", mIndex=" + mIndex
				+ ", mFavoriteFruit=" + mFavoriteFruit + ", mLatitude="
				+ mLatitude + ", mCompany=" + mCompany + ", mEmail=" + mEmail
				+ ", mPicture=" + mPicture + ", mTags=" + mTags
				+ ", mRegistered=" + mRegistered + ", mEyeColor=" + mEyeColor
				+ ", mPhone=" + mPhone + ", mAddress=" + mAddress
				+ ", mFriends=" + mFriends + ", mIsActive=" + mIsActive
				+ ", mAbout=" + mAbout + ", mBalance=" + mBalance + ", mName="
				+ mName + ", mGender=" + mGender + ", mAge=" + mAge
				+ ", mGreeting=" + mGreeting + ", mLongitude=" + mLongitude
				+ ", mId=" + mId + "]";
	}
	
	

}
