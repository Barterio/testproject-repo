package ua.barterio.testproject.utils;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ua.barterio.testproject.objects.Friends;
import ua.barterio.testproject.objects.MainObject;
import ua.barterio.testproject.objects.Tags;
import android.util.Log;

public class JsonParser {
	private static final String TAG = "JsonParser";

	public ArrayList<MainObject> getMainObjectsList(String jsonString) {
		ArrayList<MainObject> resultList = new ArrayList<MainObject>();
		if (jsonString != null && jsonString.length() > 0) {
			try {
				JSONArray mainArray = new JSONArray(jsonString);
				for (int i = 0; i < mainArray.length(); i++) {
					JSONObject jObj = mainArray.getJSONObject(i);
					MainObject mObj = new MainObject();
					mObj.setGuid(jObj.getString(MainObject.GUID));
					mObj.setIndex(jObj.getInt(MainObject.INDEX));
					mObj.setFavoriteFruit(jObj
							.getString(MainObject.FAVORITE_FRUIT));
					mObj.setLatitude(jObj.getLong(MainObject.LATITUDE));
					mObj.setCompany(jObj.getString(MainObject.COMPANY));
					mObj.setEmail(jObj.getString(MainObject.EMAIL));
					mObj.setPicture(jObj.getString(MainObject.PICTURE));
					JSONArray jTags = jObj.getJSONArray(MainObject.TAGS);
					for (int j = 0; j < jTags.length(); j++) {
						mObj.addTag(new Tags(jTags.getString(j)));
					}
					mObj.setRegistered(jObj.getString(MainObject.REGISTERED));
					mObj.setEyeColor(jObj.getString(MainObject.EYE_COLOR));
					mObj.setPhone(jObj.getString(MainObject.PHONE));
					mObj.setAddress(jObj.getString(MainObject.ADDRESS));
					JSONArray jFriends = jObj.getJSONArray(MainObject.FRIENDS);
					for (int k = 0; k < jFriends.length(); k++) {
						JSONObject jFriendsObj = jFriends.getJSONObject(k);
						mObj.addFriend(new Friends(
								jFriendsObj.getInt(Friends.ID), 
								jFriendsObj.getString(Friends.NAME)));
					}
					mObj.setIsActive(jObj.getBoolean(MainObject.IS_ACTIVE));
					mObj.setAbout(jObj.getString(MainObject.ABOUT));
					mObj.setBalance(jObj.getString(MainObject.BALANCE));
					mObj.setName(jObj.getString(MainObject.NAME));
					mObj.setGender(jObj.getString(MainObject.GENDER));
					mObj.setAge(jObj.getInt(MainObject.AGE));
					mObj.setGreeting(jObj.getString(MainObject.GREETING));
					mObj.setLongitude(jObj.getLong(MainObject.LONGITUDE));
					mObj.setId(jObj.getString(MainObject.ID));
					Log.e(TAG, mObj.toString());

					resultList.add(mObj);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return resultList;
	}

}
