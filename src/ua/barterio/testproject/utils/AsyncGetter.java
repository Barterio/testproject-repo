package ua.barterio.testproject.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

public abstract class AsyncGetter extends AsyncTask<String, String, String> {
	public abstract void onPostExecute(String result);
//	abstract void onProgressUpdate();
		public AsyncGetter() {
		}
		@Override
		protected String doInBackground(String... params) {
			byte[] result = null;
			String str = String.valueOf(Error.DATA_ERROR);
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(params[0]);
			try {
				HttpResponse response = client.execute(post);
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {
					result = EntityUtils.toByteArray(response.getEntity());
					str = new String(result, "UTF-8");
					Log.i(this.getClass().getSimpleName(), "OUTPUT: " + str);
				} else {
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (Exception e) {
			}
			return str;
		}
}
